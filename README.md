# Thingstream Click Python

A python library to communicate with the Thingstream Click module from MikroE.

Compatible with CPython/PySerial (For Windows/Linux/Raspberry Pi) and MicroPython/uart (For ESP32, etc)

## Features

* Same interface for CPython and MicroPython
	* Develop on PC with CPpthon and deploy on a microcontroller with MicroPython
* Implementation of all AT commands as methods (currently only sending)
* Module simulation for application development without hardware
* Easy implementation of other serial libraries

### Planed

* higher level abstraction from AT to MQTT
* parsing of received messages

## Usage

`from thingstream_click import ThingstreamClick`

### Serial Communication

For PySerial:

```python
from thingstream_click.pyserial import SerialInterfacePySerial

serial_interface = SerialInterfacePySerial("/dev/ttyACM0",115200,10)
```

For uart (on ESP32, UART 2)

```python
from thingstream_click.uart import SerialInterfaceMicroPython

serial_interface = SerialInterfaceMicroPython(2,115200,10)
```

For the simulator

```python
from thingstream_click import SerialInterfaceThingstreamDummy

serial_interface = SerialInterfaceThingstreamDummy("foo",115200,10) # three arguments are necessary but ignored
```

### Thingstream

```python
click = ThingstreamClick(serial_interface, debug_mode=True)
click.init()
click.at_iot_create()
click.at_iot_connect(True) # drop messages
click.at_iot_publish("test/topic","payload",1) # QOS 1
click.at_iot_disconnect()
click.at_iot_destroy()
```

## Architectual Overview

The main class is defined in `thingstream_click/__init__.py`. As well as the `SerialInterface` abstract class and on of its implementations, the `SerialInterfaceThingstreamDummy` class.

The implementation of `SerialInterface` for PySerial is in `thingstream_click/pyserial.py` and the one for Micropython's uart is in `thingstream_click/uart.py`.
